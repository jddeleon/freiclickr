from django import forms
from courses.models import Course

#allows instructor to create a course
class CourseForm(forms.ModelForm):
    title = forms.CharField(max_length=255, widget=forms.TextInput(
                                   attrs={'placeholder': 'Course Title',
                                          'autofocus': 'autofocus'}),
                            label='')

    #instructor = forms.CharField(widget=forms.HiddenInput())

    class Meta:
        model = Course
        fields = ('title', )
        exclude = ('instructor',)

# allows a student to join a course that an instructor has already created
class JoinCourseForm(forms.Form):
        key = forms.CharField(min_length=8, max_length=8, widget=forms.TextInput(
                                   attrs={'placeholder': 'Enter Course Key from Instructor',
                                          'autofocus': 'autofocus'}),
                                    label='')

        def clean_key(self):
            cleaned_data = super(JoinCourseForm, self).clean()
            key = cleaned_data.get("key")

            try:
                Course.objects.get(key=key)
            except Course.DoesNotExist:
                raise forms.ValidationError("The course key you entered was not valid. "
                                            "Please enter the key provided by your instructor.")
            return key

