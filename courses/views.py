from django.shortcuts import render
from courses.models import Course
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
from courses.forms import CourseForm, JoinCourseForm
from django.http import HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse, reverse_lazy
from django.views.generic import CreateView, DeleteView
from django.contrib import messages

@login_required()
def index(request):

    user = request.user
    create_course_form = CourseForm
    join_course_form = JoinCourseForm

    if user.groups.filter(name='Instructors').exists():
        if not user.course_instructor.all():
            return add_course(request)
    else:
        # user is a student. If student hasn't added any courses, then let him join one
        if not user.course_students.all():
            return join_course(request)

    return render(request, 'course/index.html', locals())

def course_detail(request, pk):
    #TODO Course Detail
    pass


class CourseDelete(DeleteView):
    model = Course
    success_url = reverse_lazy('courses_index')
    # template_name_suffix = '_confirm_delete' # this is the default suffix

    def get_object(self, queryset=None):
        # Hook to ensure object is owned by request.user
        obj = super(CourseDelete, self).get_object()
        if not obj.instructor == self.request.user:
            raise Http404

        return obj

@login_required()
def add_course(request):
    print "add_course"
    if request.method == 'POST':
        form = CourseForm(request.POST)
        print "adding_course"

        # Have we been provided with a valid form?
        if form.is_valid():
            print "is valid"
            course = form.save(commit=False)
            course.instructor = request.user
            course.save()
            return HttpResponseRedirect(reverse('courses_index'))
        else:
            print "contained errors"
            # form contained errors
            print form.errors
    else:
        print "display form"
        # if request was not a POST, display the form
        form = CourseForm()

    print "no form supplied"
    # Bad form (or form details), no form supplied...
    # Render the form with error messages (if any).
    return render(request, 'course/index.html', {'create_course_form': form})

@login_required()
def join_course(request, url_key=None):

    if request.method == 'POST':
        form = JoinCourseForm(request.POST)
        student = request.user

        # Have we been provided with a valid form?
        if form.is_valid():
            key = form.clean_key()
            course = Course.objects.get(key=key)

            if not student in course.students.all():
                course.students.add(student)
                messages.add_message(request, messages.SUCCESS, 'You successfully joined the course %s!' % course.title)
            else: # student is already in the course
                messages.add_message(request, messages.ERROR, 'You are already enrolled in %s!' % course.title)

            return HttpResponseRedirect(reverse('courses_index'))

        else:
            messages.add_message(request, messages.ERROR, 'Please check the course key and try again.')
            print form.errors
    else:

        if url_key: # user is trying to join using a URL
            student = request.user
            course = Course.objects.get(key=url_key)
            if not student in course.students.all():
                course.students.add(student)
                messages.add_message(request, messages.SUCCESS, 'You successfully joined the course %s!' % course.title)
            else: # student is already in the course
                messages.add_message(request, messages.ERROR, 'You are already enrolled in %s!' % course.title)
            return HttpResponseRedirect(reverse('courses_index'))
        # if request was not a POST, display the form
        form = JoinCourseForm()

    # Bad form (or form details), no form supplied...
    # Render the form with error messages (if any).
    return render(request, 'course/index.html', {'join_course_form': form})

#def join_with_url(request, key):

# allows an instructor to delete a course that they created
# @login_required()
# def delete_course(request, id):
#     print "delete_course"
#     try:
#         course = Course.objects.get(id=id)
#     except:
#         raise Http404
#
#     user = request.user
#
#     # make sure the course belongs to the instructor
#     if user.id != course.instructor_id:
#         raise Http404
#
#     course.delete()
#     #TODO send success message
#     return HttpResponseRedirect(reverse("courses_index"))

# allows a student to be removed from a course that they have previously joined

@login_required()
def unjoin_course(request, id):
    user = request.user

    try: # check that the course belongs to the user and the course is valid, if not, 404
        course = Course.objects.get(id=id)
        course.students.get(id=user.id)
    except:
        raise Http404

    if request.method == 'POST':
        user.course_students.remove(course)
        return HttpResponseRedirect(reverse("courses_index"))

    # else display confirm delete
    return render(request, 'course/unjoin_course.html', {'course': course})




@login_required()
def edit_course(request, id):
    print "edit_course"
    user = request.user
    course_id = id

    if request.method == 'POST':
        course = Course.objects.get(id=id)
        print request.POST
        form = CourseForm(request.POST, instance=course)

        # Have we been provided with a valid form?
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('courses_index'))

        else:
            # form contained errors
            print form.errors
    else:
        # if request was not a POST, display the form
        try:
            course = Course.objects.get(id=id)
        except:
            raise Http404

        # make sure the course belongs to the instructor
        if user.id != course.instructor_id:
            raise Http404

        form = CourseForm(instance=course)

    # Bad form (or form details), no form supplied...
    # Render the form with error messages (if any).
    return render(request, 'course/edit_course.html', {'edit_course_form': form, 'id': course_id})






