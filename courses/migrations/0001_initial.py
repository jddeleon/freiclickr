# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'Course Title')),
                ('key', models.CharField(null=True, editable=False, max_length=8, blank=True, unique=True, db_index=True)),
                ('instructor', models.ForeignKey(related_name=b'course_instructor', to=settings.AUTH_USER_MODEL)),
                ('students', models.ManyToManyField(related_name=b'course_students', null=True, to=settings.AUTH_USER_MODEL, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
