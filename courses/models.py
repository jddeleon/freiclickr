from django.db import models, IntegrityError
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
import string
import random

class Course(models.Model):
    title = models.CharField(max_length=255, verbose_name='Course Title')
    key = models.CharField(max_length=8, blank=True, editable=False, unique=True, db_index=True, null=True)
    instructor = models.ForeignKey(User, related_name="course_instructor")
    students = models.ManyToManyField(User, related_name="course_students", null=True, blank=True)

    def get_absolute_url(self):
        return reverse('course_detail', kwargs={'pk': self.pk})

    def __unicode__(self):
        return self.title

    def generate_random_key(self, length):
        return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(length))


    def save(self, *args, **kwargs):
        if not self.key:
            self.key = Course.generate_random_key(self, 8) # hard cording this length to 8, but it should grow dynamically if there
            # are collisions
            success = False
            failures = 0
            while not success:
                try:
                    super(Course, self).save(*args, **kwargs)
                except IntegrityError:
                    failures += 1
                    if failures > 5:  # Some bad shit went down and we couldn't save the record
                        raise
                    else:
                        # looks like a collision, try another random value
                        self.key = Course.generate_random_key(self, 8)
                else:
                    success = True

        super(Course, self).save(*args, **kwargs)




