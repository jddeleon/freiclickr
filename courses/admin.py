from django.contrib import admin
from courses.models import Course



class CourseAdmin(admin.ModelAdmin):
    fields = ('title', 'instructor', 'students', 'key')
    readonly_fields = ('key',)

admin.site.register(Course, CourseAdmin)