from django.db import models
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType


# Created these models so that I could more easily model a Course as having
# a single instructor but multiple students.
#Commenting out to try using related name instead
#class Student(models.Model):
#    user = models.OneToOneField(User)
#
#class Instructor(models.Model):
#    user = models.OneToOneField(User)
#new_group, created = Group.objects.get_or_create(name="MyGroup")


