from django.shortcuts import render, redirect
from django.shortcuts import render_to_response, RequestContext, Http404, HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required, user_passes_test


def group_required(*group_names):
    """Requires user membership in at least one of the groups passed in."""
    def in_groups(u):
        if u.is_authenticated():
            if bool(u.groups.filter(name__in=group_names)) | u.is_superuser:
                return True
        return False
    return user_passes_test(in_groups)

@login_required()
def dashboard(request):
    user = request.user
    if user.groups.filter(name='Instructors').exists():
        return redirect('instructor_dashboard')
    else:
        return redirect('student_dashboard')

@group_required('Instructors')
def instructor_dashboard(request):
    return render(request, 'profile/instructor_dashboard.html')

@group_required('Students')
def student_dashboard(request):
    return render(request, 'profile/student_dashboard.html')

