from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from clicker.views import PollDelete
from courses.views import CourseDelete

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'clicker.views.index', name='index'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    (r'^accounts/', include('allauth.urls')),
   # url(r'^(?P<username>\w+)/$', 'profiles.views.profile'),
    url(r'^dashboard/$', 'profiles.views.dashboard'),
    url(r'^courses/$', 'courses.views.index', name='courses_index'),
    url(r'^courses/add_course/$', 'courses.views.add_course', name='add_course'),
    url(r'^courses/join_course/$', 'courses.views.join_course', name='join_course'),
    url(r'^join/(?P<url_key>\w+)/$', 'courses.views.join_course', name='join_course'),
    url(r'^courses/(?P<pk>\d+)/$', 'courses.views.course_detail', name='course_detail'),
    url(r'^courses/(?P<pk>\d+)/delete_course/$', CourseDelete.as_view(), name='delete_course'),
    url(r'^courses/unjoin_course/(?P<id>\d+)/$', 'courses.views.unjoin_course', name='unjoin_course'),
    url(r'^courses/edit_course/(?P<id>\d+)/$', 'courses.views.edit_course', name='edit_course'),
    url(r'^polls/$', 'clicker.views.polls_index', name='polls_index'),
    url(r'^polls/toggle_active/$', 'clicker.views.poll_toggle_active', name='poll_toggle_active'),
    url(r'^polls/new_poll/$', 'clicker.views.new_poll', name='new_poll'),
    url(r'^polls/edit_poll/(?P<id>\d+)/$', 'clicker.views.edit_poll', name='edit_poll'),
    url(r'^polls/(?P<pk>\d+)/delete_poll/$', PollDelete.as_view() , name='delete_poll'),
    url(r'^polls/(?P<key>\w+)/$', 'clicker.views.poll_detail', name='poll_detail'),
    #url(r'^polls/(?P<key>\w+)/toggle_active/$', 'clicker.views.poll_toggle_active', name='poll_toggle_active'),

    url(r'^polls/(?P<key>\w+)/new_poll_question/$', 'clicker.views.new_poll_question', name='new_poll_question'),
    url(r'^polls/(?P<key>\w+)/edit_poll_question/(?P<id>\d+)/$', 'clicker.views.edit_poll_question',
        name='edit_poll_question'),
    url(r'^polls/(?P<key>\w+)/question/(?P<id>\d+)/$', 'clicker.views.question_detail', name='question_detail'),
    url(r'^polls/(?P<key>\w+)/question/(?P<id>\d+)/results/$', 'clicker.views.results', name='poll_results'),
    url(r'^polls/(?P<id>\w+)/vote/$', 'clicker.views.vote', name='vote'),
    url(r'^polls/(?P<key>\w+)/edit_vote/(?P<id>\d+)/$', 'clicker.views.edit_vote', name='edit_vote'),
) #+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)