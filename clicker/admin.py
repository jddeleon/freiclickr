from django.contrib import admin
from clicker.models import Question, Vote, Choice, Poll

class PollAdmin(admin.ModelAdmin):
    fields = ('name', 'instructor', 'course', 'key', 'active',
              'anonymous', 'allow_vote_change', 'max_vote_change', 'created_at', 'updated_at',)
    readonly_fields = ('key', 'created_at', 'updated_at',)

admin.site.register(Question)
admin.site.register(Vote)
admin.site.register(Choice)
admin.site.register(Poll, PollAdmin)