from django.shortcuts import render
from clicker.forms import NewPollForm, QuestionForm, ChoiceForm, VoteForm, EditPollForm
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import get_object_or_404, redirect
from clicker.models import Poll, Question, Vote, Choice
from django.forms.formsets import formset_factory
from django.forms.models import modelformset_factory, inlineformset_factory
from django.views.generic import CreateView, DeleteView
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core import serializers
import json
import string
from courses.models import Course
from django.contrib import messages

def index(request):
    template = 'index.html'
    context = locals()
    return render(request, template, context)

@login_required()
def polls_index(request):
    print "polls_index"
    user = request.user
    new_poll_form = NewPollForm(request=request)

    # Instructor
    if user.groups.filter(name='Instructors').exists():
        if not user.poll_set.all():
            return new_poll(request)
    else: #Student
        student_polls = Poll.objects.filter(course__students__id=user.id, active=True)

    return render(request, 'clicker/poll/index.html', locals())


def new_poll(request):
    if request.method == 'POST':
        form = NewPollForm(request.POST, request=request)
        print "adding_poll"

        # Have we been provided with a valid form?
        if form.is_valid():
            print "is valid"
            poll = form.save(commit=False)
            poll.instructor = request.user
            poll.save()
            return HttpResponseRedirect(reverse('polls_index'))
        else:
            print "contained errors"
            # form contained errors
            print form.errors
    else:
        print "display form"
        # if request was not a POST, display the form
        form = NewPollForm(request=request)

    print "no form supplied"
    # Bad form (or form details), no form supplied...
    # Render the form with error messages (if any).
    return render(request, 'clicker/poll/index.html', {'new_poll_form': form})

def poll_detail(request, key):
    print "render poll_detail"
    poll = get_object_or_404(Poll, key=key)
    poll_key = key
    active = poll.active

    # Add Question Forms
    question_set = poll.question_set.all()
    question_form = QuestionForm()
    question = Question()
    ChoiceFormSet = modelformset_factory(Choice, can_delete=False, form=ChoiceForm, extra=2)
    choice_formset = ChoiceFormSet(queryset=Choice.objects.none())

    action_url = reverse('new_poll_question', kwargs={'key': poll_key})

    # Vote Form
    #form = VoteForm(choices)
    VoteFormSet = modelformset_factory(Vote, form=VoteForm)
    vote_formset = VoteFormSet(queryset=question_set)





    context = {'question_form': question_form,
               'choice_formset': choice_formset,
               'poll_key': poll_key,
               'poll': poll,
               'question_set': question_set,
               'action_url': action_url,
               #'vote_formset': vote_formset,
               'active': active}

    return render(request, 'clicker/poll/detail.html', context)

def new_poll_question(request, key):
    print "New Question"
    print "key = %s" %(key)
    ChoiceFormSet = modelformset_factory(Choice, form=ChoiceForm)
    poll = get_object_or_404(Poll, key=key)

    if request.method == 'POST':
        choice_formset = ChoiceFormSet(request.POST)
        question_form = QuestionForm(request.POST)

        # Have we been provided with a valid form?
        if question_form.is_valid() and choice_formset.is_valid():
            print "formset is valid"
            question = question_form.save(commit=False)
            question.poll = poll
            question.save()

            choices = choice_formset.save(commit=False)

            for choice in choices:
                choice.question = question
                choice.save()

            return HttpResponseRedirect(reverse('poll_detail', args=[key]))
        else:
            print "contained errors"
            # form contained errors
            print question_form.errors
    else:
        print "display form"
        # if request was not a POST, display the form
        question_form = QuestionForm()

    print "no form supplied"
    # Bad form (or form details), no form supplied...
    # Render the form with error messages (if any).
    return render(request, 'clicker/poll/detail.html', {'question_form': question_form})

def edit_poll_question(request, key, id):
    #TODO fix edit question to delete items
    print "edit_question"
    user = request.user
    poll = get_object_or_404(Poll, key=key)
    question = get_object_or_404(poll.question_set, id=id)
    question_id = id
    poll_key = key
    print "1"
    if not poll.instructor == user:
        raise Http404
    print "2"


    if request.method == 'POST':

        ChoiceFormSet = modelformset_factory(Choice, form=ChoiceForm)
        question_form = QuestionForm(request.POST, instance=question)
        choice_formset = ChoiceFormSet(request.POST)

        print "Choice formset"
        print choice_formset
        # Have we been provided with a valid form?
        if question_form.is_valid() and choice_formset.is_valid():
            question_form.save()
            choices = choice_formset.save(commit=False)

            for choice in choices:
                choice.question = question
                choice.save()


            return HttpResponseRedirect(reverse('poll_detail', args=[key]))

        else:
            # form contained errors
            print question_form.errors
            print choice_formset.errors
    else:
        # request was GET, so display the form
        question_form = QuestionForm(instance=question)
        ChoiceFormSet = modelformset_factory(Choice, can_delete=True, form=ChoiceForm, extra=0)
        choice_formset = ChoiceFormSet(queryset=question.choice_set.all())

    action_url = reverse('edit_poll_question', kwargs={'key': poll_key, 'id': question_id})

    context = {'question_form': question_form,
               'choice_formset': choice_formset,
               'poll_key': poll_key,
               'poll': poll,
               'action_url': action_url}
    # Bad form (or form details), no form supplied...
    # Render the form with error messages (if any).
    return render(request, 'clicker/poll/edit_poll_question.html', context)

def vote(request, id):
    print "Vote"
    question = get_object_or_404(Question, id=id)
    poll = question.poll
    student = request.user


    if not poll.active:
        messages.add_message(request, messages.ERROR, 'Sorry, that poll is no longer active')
        return HttpResponseRedirect(reverse('polls_index'))


    if request.method == 'POST':
        vote_form = VoteForm(request.POST, question=question)

        if vote_form.is_valid():
            vote = vote_form.save(commit=False)
            vote.question = question
            vote.student = student
            vote.save()
            messages.add_message(request, messages.SUCCESS, "Your response was successfully counted!")

    else:
        print "GET request"
        raise Http404
        # vote_form = VoteForm(question=question)

    return HttpResponseRedirect(reverse('poll_detail', args=[poll.key]))


# def poll_toggle_active(request, key):
#     poll = get_object_or_404(Poll, key=key)
#
#     if not request.user == poll.instructor:
#         raise Http404
#
#     print poll.active
#     poll.active = not poll.active
#     poll.save()
#
#     return HttpResponseRedirect(reverse('poll_detail', args=[key]))

def poll_toggle_active(request):
    poll_key = None
    poll = None
    print "ajax request"

    if request.method == 'GET':
        poll_key = request.GET['key']

    if poll_key:
        poll = Poll.objects.get(key=str(poll_key))
        if poll:
            poll.active = not poll.active
            poll.save()

    active = str(poll.active).lower()
    print "poll is now %s" % active
    return HttpResponse(active)

class PollDelete(DeleteView):
    model = Poll
    success_url = reverse_lazy('polls_index')
    #template_name = 'course/../templates/courses/course_confirm_delete.html'  # this is the default suffix

    def get_object(self, queryset=None):
        # Hook to ensure object is owned by request.user
        obj = super(PollDelete, self).get_object()
        if not obj.instructor == self.request.user:
            raise Http404

        return obj

def question_detail(request, key, id):
    print "question detail"
    print key
    poll = get_object_or_404(Poll, key=key)
    user = request.user
    question = get_object_or_404(Question, id=id)


    form = VoteForm(question=question)

    return render(request, 'clicker/poll/question_detail.html', locals())

def results(request, key, id):
    poll = get_object_or_404(Poll, key=key)
    question = get_object_or_404(Question, id=id)
    votes = question.vote_set.all()
    choice_set = question.choice_set.all()
    categories = []
    vote_count = []

    if not request.user == poll.instructor:
        raise Http404

    for choice in choice_set:
        categories.append(choice.choice_text)
        vote_count.append(choice.vote_set.count())


    categories = json.dumps(categories)
    vote_count = json.dumps(vote_count)

    students = poll.course.students.all()
    student_votes = {}

    for student in students:
        try:
            student_votes[student] = student.vote_set.get(question__id=id)

        except Vote.DoesNotExist:
            print "print None"
            student_votes[student] = None



    context = {'poll': poll,
               'question': question,
               'categories': categories,
               'vote_count': vote_count,
               'choice_set': choice_set,
               'student_votes': student_votes,
               'students': students}

    return render(request, 'clicker/poll/results.html', context)

@login_required()
def edit_poll(request, id):
    print "edit_poll"
    user = request.user
    poll = get_object_or_404(Poll, id=id)
    poll_id = poll.id

    # make sure the poll belongs to the instructor
    if user.id != poll.instructor_id:
        raise Http404

    if request.method == 'POST':
        poll = Poll.objects.get(id=id)
        print request.POST
        form = EditPollForm(request.POST, instance=poll)

        # Have we been provided with a valid form?
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('polls_index'))

        else:
            # form contained errors
            print form.errors

    else:
        # if request was not a POST, display the form
        poll = get_object_or_404(Poll, id=id)


    form = EditPollForm(instance=poll)

    # Bad form (or form details), no form supplied...
    # Render the form with error messages (if any).
    return render(request, 'clicker/poll/edit_poll.html', {'form': form, 'id': poll_id})

@login_required()
def edit_vote(request, key, id):
    print "edit_poll"
    user = request.user
    poll = get_object_or_404(Poll, key=key)
    poll_id = poll.id
    course = Course.objects.get(poll__id=poll.id)
    question = get_object_or_404(Question, id=id)
    # make sure student can vote in this poll
    if user not in course.students.all():
        raise Http404

    if request.method == 'POST':
        vote = get_object_or_404(Vote, student__id=user.id, question__id=question.id)
        print request.POST
        form = VoteForm(request.POST, instance=vote, question=question)

        # Have we been provided with a valid form?
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, "Your vote was successfully changed")
            return HttpResponseRedirect(reverse('poll_detail', args=[poll.key]))

        else:
            # form contained errors
            print form.errors
            messages.add_message(request, messages.ERROR, "Please check the form and try again")

    else:
        # if request was not a POST, display the form
        vote = get_object_or_404(Vote, student__id=user.id, question__id=id)


    form = VoteForm(instance=vote, question=question)

    # Bad form (or form details), no form supplied...
    # Render the form with error messages (if any).
    return render(request, 'clicker/vote/edit_vote.html', {'form': form, 'id': vote.id, 'poll_key': poll.key, 'question_id': question.id})


