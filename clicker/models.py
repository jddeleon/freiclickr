from django.db import models, IntegrityError
from django.contrib.auth.models import User
from courses.models import Course
from django.core.urlresolvers import reverse
import random
import string
import datetime

class Poll(models.Model):
    name = models.CharField(max_length=255, default="Unnamed Poll")
    key = models.CharField(max_length=16, blank=True, editable=False, unique=True, db_index=True, null=True)
    instructor = models.ForeignKey(User)
    course = models.ForeignKey(Course)
    active = models.BooleanField(default=False)
    anonymous = models.BooleanField(default=True, help_text="Allow votes to be anonymous?")
    allow_vote_change = models.BooleanField(default=False, help_text="Allow students to change their vote?")
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated_at = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('poll_detail', kwargs={'pk': self.pk})

    def generate_random_key(self, length):
        return ''.join(random.choice(string.ascii_letters) for _ in range(length))

    def save(self, *args, **kwargs):
        if not self.key:
            self.created_at = datetime.datetime.today()
            self.key = Poll.generate_random_key(self, 16) # hard cording this length to 16, but it should grow dynamically if there are collisions
            success = False
            failures = 0
            while not success:
                try:
                    super(Poll, self).save(*args, **kwargs)
                except IntegrityError:
                    failures += 1
                    if failures > 5:  # Some bad shit went down and we couldn't save the record
                        raise
                    else:
                        # looks like a collision, try another random value
                        self.key = Poll.generate_random_key(self, 16)
                else:
                    success = True

        self.updated_at = datetime.datetime.today()
        super(Poll, self).save(*args, **kwargs)



class Question(models.Model):
    question_text = models.CharField(max_length=255, verbose_name='Poll Question')
    poll = models.ForeignKey(Poll)
    created_at = models.DateTimeField(editable=False)
    updated_at = models.DateTimeField()

    def save(self, *args, **kwargs):
        # On save, update timestamps
        if not self.id:
            self.created_at = datetime.datetime.today()

        self.updated_at = datetime.datetime.today()
        return super(Question, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.question_text


class Choice(models.Model):
    question = models.ForeignKey(Question)
    choice_text = models.CharField(max_length=255, verbose_name='Response Choice')
    created_at = models.DateTimeField(editable=False)
    updated_at = models.DateTimeField()

    def save(self, *args, **kwargs):
        # On save, update timestamps
        if not self.id:
            self.created_at = datetime.datetime.today()

        self.updated_at = datetime.datetime.today()
        return super(Choice, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.choice_text

class Vote(models.Model):
    question = models.ForeignKey(Question)
    choice = models.ForeignKey(Choice)
    #ip = models.GenericIPAddressField(verbose_name='Student\'s IP Address')
    student = models.ForeignKey(User)
    has_changed = models.BooleanField(default=False)
    times_changed = models.PositiveSmallIntegerField(default=0)
    created_at = models.DateTimeField(editable=False)
    updated_at = models.DateTimeField()


    def save(self, *args, **kwargs):
        # On save, update timestamps, has_changed
        if self.id:
            self.has_changed = True
            self.times_changed += 1
        else:
            self.created_at = datetime.datetime.today()

        self.updated_at = datetime.datetime.today()
        return super(Vote, self).save(*args, **kwargs)

    def __unicode__(self):
        return str(self.choice)