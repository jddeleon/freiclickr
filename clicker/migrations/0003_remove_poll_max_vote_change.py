# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clicker', '0002_remove_vote_ip'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='poll',
            name='max_vote_change',
        ),
    ]
