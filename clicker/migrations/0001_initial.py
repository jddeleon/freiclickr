# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('courses', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Choice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('choice_text', models.CharField(max_length=255, verbose_name=b'Response Choice')),
                ('created_at', models.DateTimeField(editable=False)),
                ('updated_at', models.DateTimeField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Poll',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'Unnamed Poll', max_length=255)),
                ('key', models.CharField(null=True, editable=False, max_length=16, blank=True, unique=True, db_index=True)),
                ('active', models.BooleanField(default=False)),
                ('anonymous', models.BooleanField(default=True, help_text=b'Allow votes to be anonymous?')),
                ('allow_vote_change', models.BooleanField(default=False, help_text=b'Allow students to change their vote?')),
                ('max_vote_change', models.PositiveSmallIntegerField(default=0, help_text=b'Maximum number of times a student can change his/her vote')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('course', models.ForeignKey(to='courses.Course')),
                ('instructor', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question_text', models.CharField(max_length=255, verbose_name=b'Poll Question')),
                ('created_at', models.DateTimeField(editable=False)),
                ('updated_at', models.DateTimeField()),
                ('poll', models.ForeignKey(to='clicker.Poll')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Vote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ip', models.GenericIPAddressField(verbose_name=b"Student's IP Address")),
                ('has_changed', models.BooleanField(default=False)),
                ('times_changed', models.PositiveSmallIntegerField(default=0)),
                ('created_at', models.DateTimeField(editable=False)),
                ('updated_at', models.DateTimeField()),
                ('choice', models.ForeignKey(to='clicker.Choice')),
                ('question', models.ForeignKey(to='clicker.Question')),
                ('student', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='choice',
            name='question',
            field=models.ForeignKey(to='clicker.Question'),
            preserve_default=True,
        ),
    ]
