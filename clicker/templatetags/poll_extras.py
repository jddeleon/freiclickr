from django import template
from clicker.models import Question, Vote, Poll, Choice
from django.contrib.auth.models import User


register = template.Library()

@register.filter(name='has_voted')
def has_voted(user, question):
    voted = user.vote_set.filter(question__id=question.id)

    return True if voted else False

