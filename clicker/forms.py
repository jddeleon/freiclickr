from django import forms
from django.forms.models import inlineformset_factory, BaseInlineFormSet, modelformset_factory
from django.contrib.auth.models import User, Group
from clicker.models import Poll, Question, Choice, Vote


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ('question_text',)

    question_text = forms.CharField(max_length=255,
                                    widget=forms.TextInput(attrs={'placeholder': 'Question Text',
                                                                  'autofocus': 'autofocus'}),
                                    label='')


class ChoiceForm(forms.ModelForm):
    class Meta:
        model = Choice
        fields = ('choice_text',)
        exclude = ('updated_at',)

    choice_text = forms.CharField(max_length=255,
                                    widget=forms.TextInput(attrs={'placeholder': 'Choice Text',
                                                                  'autofocus': 'autofocus',
                                                                  'class': 'form-control'}),
                                    label='')

class VoteForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.question = kwargs.pop('question')
        super(VoteForm, self).__init__(*args, **kwargs)
        self.fields['choice'].queryset =self.question.choice_set.all()

    class Meta:
        model = Vote
        fields = ('choice',)
        exclude = ('updated_at',)
        #widgets = {'choice': forms.RadioSelect()}

    choice = forms.ModelChoiceField(widget=forms.RadioSelect(),
                                    empty_label=None,
                                    queryset=None)

class EditVoteForm(forms.ModelForm):

    class Meta:
        model = Vote
        fields = ('choice',)
        exclude = ('updated_at',)
        #widgets = {'choice': forms.RadioSelect()}

    choice = forms.ModelChoiceField(widget=forms.RadioSelect(),
                                    empty_label=None,
                                    queryset=None)




class NewPollForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(NewPollForm, self).__init__(*args, **kwargs)
        self.fields['course'].queryset =self.request.user.course_instructor.all()

    name = forms.CharField(max_length=255, widget=forms.TextInput(
                                   attrs={'placeholder': 'Poll Name',
                                          'autofocus': 'autofocus'}),
                            label='')

    class Meta:
        model = Poll
        fields = ('name', 'course', 'anonymous', 'allow_vote_change', )
        exclude = ('instructor', 'active', 'created_at', 'updated_at',)

class EditPollForm(forms.ModelForm):

    name = forms.CharField(max_length=255, widget=forms.TextInput(
                                   attrs={'placeholder': 'Poll Name',
                                          'autofocus': 'autofocus'}),
                            label='Poll Name')

    class Meta:
        model = Poll
        fields = ('name', 'allow_vote_change',)
        exclude = ('instructor', 'active', 'created_at', 'updated_at', 'course',)




class SignupForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = ''
        self.fields['password1'].label = ''
        self.fields['password2'].label = ''
        self.fields['password2'].widget.attrs['placeholder'] = 'Password (again)'
        self.fields['email'].label = ''

    USER_TYPES = [('Students', 'Student'), ('Instructors', 'Instructor')]
    groups = forms.ChoiceField(choices=USER_TYPES, widget=forms.RadioSelect(), label="Select your account type")

    first_name = forms.CharField(max_length=30,
                                 widget=forms.TextInput(
                                   attrs={'placeholder':
                                          'First Name',
                                          'autofocus': 'autofocus'}),
                                 label='')
    last_name = forms.CharField(max_length=30,
                                widget=forms.TextInput(
                                   attrs={'placeholder':
                                          'Last Name'}),
                                label='')

    def signup(self, request, user):
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        g = Group.objects.get(name=self.cleaned_data['groups'])
        g.user_set.add(user)
        user.save()
